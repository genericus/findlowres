(declare (unit flrutil))
(declare (uses srfi-4))

(define (split-u8vector-at u8vec i)
  (values (subu8vector u8vec 0 i)
          (subu8vector u8vec i (u8vector-length u8vec))))

(define (decode-int u8vec)
  (call-with-values (lambda () (split-u8vector-at u8vec 2))
    (lambda (high low)
      (fxior (fxshl (decode-short high) 16) ; fxshl = fixnum shift left
             (decode-short low)))))

(define (decode-short u8vec)
  (let ((1st (u8vector-ref u8vec 0))
        (2nd (u8vector-ref u8vec 1)))
    (fxior (fxshl 1st 8)
           2nd)))
