;;;; findlowres.scm

(declare (uses posix flrjpeg flrpng))

(define (image? f)
  (and (regular-file? f)
       (or (jpeg? f) (png? f))))

(define (low-res? f minx miny)
  (if (regular-file? f)
      (let ((dim-fn (cond
                     ((jpeg? f) jpeg-dimensions)
                     ((png? f) png-dimensions)
                     (else #f))))
        (if dim-fn
            (let* ((result (dim-fn f))
                   (x (car result))
                   (y (cdr result)))
              (and (<= x minx)
                   (<= y miny)))
            #f))
      #f))

(define (find-low-res dir minx miny #!key (action cons)
                      (limit (lambda (x) (constantly #t))))
  (find-files dir #:test (lambda (f) (low-res? f minx miny))
                  #:action action
                  #:limit limit))

(define (delete-low-res dir minx miny #!key (confirm #t)
                        (limit (lambda (x) (constantly #t))))
  (define (confirm-delete f x)
    (printf "Delete file ~a ? [y/n] " f)
    (let ((answer (read-line)))
      (cond ((member answer '("y" "Y")) (delete-file f))
            ((member answer '("n" "N")) #f)
            (else (begin (printf "Please answer y or n.~%")
                         (confirm-delete f x))))))
  (find-low-res dir minx miny #:limit limit
                #:action (if confirm confirm-delete delete-file)))

(define (main)
  (define (pos-int? x)
    (and (integer? x)
         (positive? x)))
  (define (read-arg a)
    (with-input-from-string a read))
  (let ((args (command-line-arguments)))
    (if (not (= (length args) 4))
        (printf "Requires four arguments: directory minx miny recurse?~%")
        (let ((d (first args))
              (minw (read-arg (second args)))
              (minh (read-arg (third args)))
              (recur (read-arg (fourth args))))
          (if (and (directory? d)
                   (pos-int? minw)
                   (pos-int? minh)
                   (boolean? recur))
              (delete-low-res d minw minh
                              #:confirm #t
                              #:limit (if recur
                                          (constantly #t)
                                          0))
              (printf "Bad arguments. Use, for example, \"findlowres ./ 300 300 #f~%\""))))))

(main)
