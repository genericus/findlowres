;;;; jpeg.scm

(declare (unit flrjpeg))
(declare (uses srfi-1 srfi-4 flrutil))

(define (jpeg? f)
  (let ((header
         (call-with-input-file f
           (lambda (in)
             (read-u8vector 2 in)))))
    (and (fx= (u8vector-length header) 2)
         ;; Check for the JPEG header every proper JPEG has:
         (equal? header '#u8(255 216)))))

(define (jpeg-find in)
  (define sof-markers (append (iota 3 #xC0)
                              (iota 2 #xC5)
                              (iota 2 #xC9)
                              (iota 2 #xCD)))
  (define (read-until-mark)
    ;; Looks for a JPEG marker: 0xFF
    (if (fx= (u8vector-ref (read-u8vector 1 in) 0) #xFF)
        ;; Found #xFF, skip repeats and return next value
        (do ((c (u8vector-ref (read-u8vector 1 in) 0)
                (u8vector-ref (read-u8vector 1 in) 0)))
            ((not (fx= c #xFF)) c)
          #t)
        (read-until-mark)))

  (define (find-w-h w h)
    (let ((marker (read-until-mark)))
      (cond
       ((member marker sof-markers)
        (begin
          (read-u8vector 3 in)          ; Skip frame length
          (let ((h (read-u8vector 2 in))
                (w (read-u8vector 2 in)))
            (find-w-h w h))))
       ((or (fx= marker #xD9) (fx= marker #xDA))
        (cons w h))                     ; EOF
       (else
        (begin (read-u8vector (fx- (decode-short (read-u8vector 2 in)) 2) in)
               (find-w-h w h))))))      ; Seek ahead by frame length

  (read-u8vector 2 in)                  ; Skip 2 byte beginning
  (find-w-h 0 0))


(define (jpeg-dimensions f)
  (let* ((raw-out
          (call-with-input-file f
            jpeg-find))
         (encoded-width (decode-short (car raw-out)))
         (encoded-height (decode-short (cdr raw-out))))
    (cons encoded-width encoded-height)))
