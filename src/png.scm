;;;; png.scm

(declare (unit flrpng))
(declare (uses srfi-4 flrutil))

(define (png? f)
  (let ((header
         (call-with-input-file f
           (lambda (in)
             (read-u8vector 8 in)))))
    (and (fx= (u8vector-length header) 8)
         ;; Check for the PNG header every proper PNG has:
         (equal? header '#u8(137 80 78 71 13 10 26 10)))))

(define (png-dimensions f)
  ;; Change 4 bytes into one number
  (let* ((raw-out
          (call-with-input-file f
            (lambda (in)
              (subu8vector (read-u8vector 28 in) 16 28))))
         (encoded-width (subu8vector raw-out 0 4))
         (encoded-height (subu8vector raw-out 4 8)))
    (if (fx= (u8vector-length raw-out) 12)
        (cons (decode-int encoded-width)
              (decode-int encoded-height))
        (error (conc "Error reading " f)))))
